# -*- coding: utf-8 -*-

import logging
import traceback
import json

from telegram import Bot, ParseMode
from telegram.utils.request import Request
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

from datetime import time

import config

import db
import filters

if config.use_proxy:
    req = Request(proxy_url=config.proxy,
                  urllib3_proxy_kwargs={'username':config.proxy_username,
                                        'password':config.proxy_password})
else:
    req = Request()

bot = Bot(config.token, request=req)
upd = Updater(bot=bot, use_context=True)
dp = upd.dispatcher

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)


def error(update, context):
    logger = logging.getLogger()
    update_text = json.dumps(update.to_dict() if update else None, indent=2, ensure_ascii=False)
    warn_update = f'Update: "{update_text}"'
    warn_error = f'Error: {context.error.__class__.__name__} "{context.error}"\n\n' \
            f'Traceback:\n{str().join(traceback.format_tb(context.error.__traceback__))}'
    logger.warning(warn_update)
    logger.warning(warn_error)
    bot.send_message(chat_id=config.errorlog,
                     text=warn_update)
    bot.send_message(chat_id=config.errorlog,
                     text=warn_error)


dp.add_error_handler(error)

def verbalize_chat(update, context):
    update.message.reply_text(str(update.effective_chat.id))


dp.add_handler(CommandHandler('verbalize', verbalize_chat))

dp.add_handler(CommandHandler('export',
                              filters.admin(db.export)))


def format_stat(stat):
    return (f"За последние *{stat['hours']} часа*:\n" +
            f"  Новых пользователей: *{stat['new_users']}*\n" +
            f"  Пользователей взаимодействовало с ботом: *{stat['updated_users']}*\n"
            f"  Взаимодействий с ботом: *{stat['updates']}*\n"
            f"Всего пользователей бота: *{stat['total_users']}*")


def statistics_job(context):
    stat = db.count_last_hours()
    context.bot.send_message(chat_id=config.admin,
                             text=format_stat(stat),
                             parse_mode=ParseMode.MARKDOWN)


jq = upd.job_queue
jq.run_daily(statistics_job, time(hour=15))


def statistics_cb(update, context):
    try:
        h = int(update.message.text.split()[1])
    except:
        h = None
    stat = db.count_last_hours(h) if h is not None else db.count_last_hours()
    update.message.reply_text(format_stat(stat),
                              parse_mode=ParseMode.MARKDOWN)


dp.add_handler(CommandHandler('stat', statistics_cb))
