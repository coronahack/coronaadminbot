from telegram.ext import BaseFilter

import config


class Command(BaseFilter):
    def __init__(self, command_text):
        super().__init__()
        self.cmd = command_text

    def filter(self, message):
        if message and message.text:
            return message.text.startswith('/{0}'.format(self.cmd))
        else:
            return False


def admin(cb):
    def new_cb(update, context):
        if update.effective_chat.id == int(config.admin):
            return cb(update, context)
    return new_cb
