from telegram import Update
from telegram.ext import CallbackContext

from pymongo import MongoClient
from pandas.io.json import json_normalize
import config

import statistics
import toolz

db = MongoClient(config.mongo_url)[config.database_name]


def export_collection(collection_name):
    """
    simple export procedure that exports data from mongo collection using pandas
    https://gist.github.com/mieitza/5d35d0a4f2671127f7120c75c8764385
    :return: csv filename
    """
    fn = "{0}.csv".format(collection_name)
    cursor = db[collection_name].find()
    df = json_normalize(list(cursor))
    if '_id' in df: del df['_id']
    df.to_csv(fn)
    return fn


def check_collection(collection_name):
    t = db[collection_name].find_one()
    return t is not None


def export(update: Update, context: CallbackContext):
    collection_name = update.message.text[len('/export '):]
    if check_collection(collection_name):
        fn = export_collection(collection_name)
        context.bot.send_document(chat_id=update.effective_chat.id,
                                  document=open(fn, 'rb'))
    else:
        update.message.reply_text("Collection is empty or doesn't exist")


count_last_hours = toolz.partial(statistics.count_last_hours, db.history)
