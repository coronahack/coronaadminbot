import user
import datetime


def dt_in_interval(dt, ti_left=None, ti_right=None):
    print(dt ,ti_left, ti_right)
    tl = (ti_left <= dt) if ti_left is not None else True
    tr = (dt <= ti_right) if ti_right is not None else True
    return tl and tr


def is_new_user(he, ti_left=None, ti_right=None):
    dt = user.parse_timestamp(he["history"][0]["ts"])
    return dt_in_interval(dt, ti_left, ti_right)


def have_updated(he, ti_left=None, ti_right=None):
    dt = user.parse_timestamp(he["history"][-1]["ts"])
    return dt_in_interval(dt, ti_left, ti_right)


def count_updates(he, ti_left=None, ti_right=None):
    in_interval = lambda h: dt_in_interval(user.parse_timestamp(h['ts']), ti_left, ti_right)
    return sum(map(in_interval, filter(lambda t: t['type'] == 'update', he['history'])))


def count(collection, ti_left=None, ti_right=None):
    stat = {"new_users": 0, "updates": 0, "updated_users": 0, "total_users": 0}
    for c in collection.find():
        stat["new_users"] += is_new_user(c, ti_left, ti_right)
        user_updates = count_updates(c, ti_left, ti_right)
        stat["updates"] += user_updates
        stat["updated_users"] += bool(user_updates)
        stat["total_users"] += 1
    return stat


def count_last_hours(collection, hours=24):
    stat = count(collection, ti_left=datetime.datetime.now() - datetime.timedelta(hours=hours))
    stat["hours"] = hours
    return stat

